import React from 'react';
import {Context} from '../Context'
import { useParams } from 'react-router-dom';
import { useContext } from 'react';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

const Filmdetails = () => {
    const {film} = useContext(Context);
    const {id} = useParams();
    
    return (
        <>
            
        <div className='container' style={{width:'10rem'}}>
        <Button variant='warning' className='d-flex container' style={{width:'6rem',text:'center',marginBottom:500}} > <Link to='/' style={{color:'white',textDecoration:'none'}}> {'<'} Retour </Link></Button> 

        </div>
        <div className='d-flex  justify-content-center align-items-center' style={{width:'100%',height:'100vh'}}>
        {
        film.filter((items)=> parseInt(items.id) === parseInt(id)).map((items,pos)=>{
        
            return(
            <Card className='m-3' style={{ width: '28rem',height:'370px', cursor:'pointer'}} key={pos} >
                <Card.Img variant="top" src={items.img} style={{height: '21rem'}}/>
                <Card.Body>
                <Card.Title > {items.title} </Card.Title>
                    <Card.Text>
                    {items.description} {' '}
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ut, sit ea ab modi tempora provident aut,
                    reiciendis voluptatem quae expedita nostrum,
                    totam quaerat vero distinctio cumque. Architecto velit repudiandae expedita?
                    </Card.Text>
                    <Card.Text style={{ fontSize: 20, }}>
                    {items.rating}
                    </Card.Text>
            </Card.Body>
        </Card>
        )
    })
    }
        </div>
        </>
    );
}

export default Filmdetails;
