import { FormGroup,Button, FormControl, Container } from 'react-bootstrap';
import { useContext } from 'react';
import { Context } from '../Context';
// import { useForm } from 'react-hook-form';
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap'

function MoviAdd() {
    const {register, handleSubmit, onSubmit,} = useContext(Context);
    
    
;  return (
    <Container className='d-flex align-items-center justify-content-between' style={{width:'100%',height:'100vh'}}>
       <Form onSubmit={handleSubmit(onSubmit)} style={{width:350,height:'470px',borderRadius:12}} className='shadow-lg container p-3 mb-5 bg-body'>
            <FormGroup className='mt-3'>
            <input type="number"  min="6" max="100" {...register('id')}/>
                {/* <FormControl type="number" defaultValue={6} placeholder={6} {...register('id')}/> */}
            </FormGroup>
            <FormGroup className='mt-3'>
                <FormControl placeholder='title' {...register('title')}/>
            </FormGroup>
            <FormGroup className='mt-3'>
                <FormControl   as={'textarea'} rows={12} placeholder='description' {...register('description')}/>
            </FormGroup>
            <FormGroup className='mt-3'>
                <FormControl type='file' placeholder='ajouter une image' {...register('img')}/>
            </FormGroup>
            <FormGroup className='mt-3'>  
                <FormControl  type='text' placeholder='* * * * *' {...register('rating')}/>
            </FormGroup>
            <FormGroup className='mt-3'>
                <Button type='submit' variant='success'> Enregistrer </Button>
            </FormGroup>
        
   </Form>
   </Container>

   
  );
}

export default MoviAdd;