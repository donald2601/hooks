import { Container } from 'react-bootstrap';
import { useContext} from 'react';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Context } from '../Context';
import './movicard.css';
// import filmA from '../filmA.jpg'
// import filmB from '../filmB.jpg'
// import filmE from '../filmE.jpg'
// import filmF from '../filmF.jpg'
// import filmG from '../filmG.jpg'
// import filmH from '../filmH.jpg'
// import filmI from '../filmI.jpg'
// import { set } from 'react-hook-form';


// import filmA from '../filmA.jpg'
// import filmB from '../filmB.jpg'
// import filmE from '../filmE.jpg'
// import filmF from '../filmF.jpg'
// import filmG from '../filmG.jpg'
// import filmH from '../filmH.jpg'
// import filmI from '../filmI.jpg'

function MovieCard() {
  const {film, setFilm} = useContext(Context);
  
//   const [film,setFilm]= useState ([
//     {id:0,
//     title:"ALPHAVILLE",
//     description:"dddddddd",
//     img : filmA,
//     rating:"* * *",},

//     {id:1,
//     title:"PLAN 9",
//     description:"dddddddd",
//     img : filmB,
//     rating:"*",},

//     {id:2,
//     title:"THEATRE DE L'OPERA",
//     description:"dddddddd",
//     img : filmE,
//     rating:"* * * * *",},

//     {id:3,
//     title:"BELLE ARCHER'S",
//     description:"dddddddd",
//     img : filmF,
//     rating:"*",},

//     {id:4,
//     title:"LA RIVIERA ITALIENNE",
//     description:"dddddddd",
//     img : filmG,
//     rating:"* * * *",},

//     {id:5,
//     title:"SEPTEMBER",
//     description:"dddddddd",
//     img : filmI,
//     rating:"* *",},
// ])

const handlDelete = (id) => {
  let newfilm = film.filter((items)=>parseInt(items.id) !== parseInt(id));
  setFilm(newfilm);
 }

  // const [position, setPosition] = useState(0);
  // const [border, setBorder] = useState('');

  // const handleChange = (pos)=> {
  //     setPosition (pos);
  // }

  return (
    <div >
    <Container className='d-flex align-items-center justify-content-start' style={{width:'100%',height:'100vh'}}>
       {
      film.map((items, pos)=>{

       
        return(
        <Card className='m-3' style={{ width: '18rem', cursor:'pointer'}} key={pos} >
          <Card.Img variant="top" src={items.img} style={{height: '21rem'}}/>
            <Card.Body>
              <Card.Title > {items.title} </Card.Title>
                <Card.Text>
                  {items.description}
                </Card.Text>
                <Card.Text style={{ fontSize: 20, }}>
                  {items.rating}
                </Card.Text>
              <Button variant="primary"  style={{width:'7rem',color:'white'}} > <Link to={`/filmdetails/${pos}`}  style={{color:'white',textDecoration:'none'}}>Lire </Link> </Button> <br/>
              <Button className='mt-3'style={{width:'7rem'}}  variant="primary" onClick={()=>{handlDelete(pos) }}> Supprimer </Button>
          </Card.Body>
    </Card>
          
    
        )
      })
    }
    </Container>

    <Container>

        <Button variant="primary m-3" style={{ width: '18rem'}}> <Link to='/moviadd' style={{textDecoration:'none', color:"white"}}> Ajouter un film depuis votre Terminal </Link> </Button>
        <Button variant="primary m-3" style={{ width: '18rem' }} >  <Link to='/movibibliotheque' style={{textDecoration:'none', color:"white"}}> Ajouter un film depuis la bibliotheque </Link>  </Button>

    </Container>

        </div>
  );
}

export default MovieCard;