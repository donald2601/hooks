import MoviAdd from './compenent/MoviAdd';
import 'bootstrap/dist/css/bootstrap.min.css';
import MoviCard from './compenent/MoviCard';
// import MovieList from './compenent/MovieList';
// import Filter from './compenent/Filter';
import { Routes, Route  } from 'react-router-dom';
import Movibibliotheque from './compenent/Movibibliotheque';
import Verification from './compenent/Verification';
import './App.css';
import Filmdetails from './compenent/Filmdetails';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path='/' element={<MoviCard/>}/>
        <Route path='/moviadd' element={<MoviAdd/>}/>
        <Route path='/movibibliotheque' element={<Movibibliotheque/>}/>
        <Route path='/verification' element={<Verification/>}/>
        <Route path='/filmdetails/:id' element={<Filmdetails/>}/>
      </Routes>
      
    </div>
  );
}

export default App;
